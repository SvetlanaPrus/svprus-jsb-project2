import '../styles/index.scss';
import {getTariff} from "./helpers/getTariff";
import {objectTitles} from "./constants";
import {clearPayments, clearPaymentsAuto} from "./helpers/clearPayments";
import {noSelection} from "./helpers/noSelection";
import {
    autoCheckServiceRight,
    // removeAllMarksService,
    uncheckServiceRight
} from "./helpers/rightNav";
import {pay} from "./helpers/rightNav_pay";
import {checkStorage} from "./helpers/checkStorage";

const divArray = document.getElementsByClassName("left__company", "left__company selected");  // this is ARRAY
const h2CenterTitle = document.querySelector(".center__title");     // element incl.tag, only ONE element
const pCenterDesc = document.querySelector(".center__desc");        // element incl.tag, only ONE element
const leftTitlesArray = document.getElementsByClassName("left__company-desc");     // this is ARRAY
let payment = {};

// RIGHT NAV.PANEL: auto-clear -
// removeAllMarksService();

// CENTER: auto-clear (Field: "К оплате") -
// clearPaymentsAuto();

checkStorage();

// LEFT SIDE: select service -
for (let i = 0; i < divArray.length; i++) {
    divArray[i].addEventListener('click', () => {

        noSelection();     // reset selection
        divArray[i].setAttribute('class', "left__company selected");
        let serviceId = divArray[i].getAttribute('data-id');
        payment.id = serviceId;     // fill in object "payment"

        for (let j = 0; j < leftTitlesArray.length; j++) {                     // Replacement of the center header
            if (i === j) h2CenterTitle.innerHTML = leftTitlesArray[j].innerHTML;
        }

        let myArray = Object.entries(objectTitles);                 // Replacement description in the center
        for (let k = 0; k < myArray.length; k++) {
            if (myArray[k][0] === serviceId) pCenterDesc.innerHTML = myArray[k][1];
        }
    });
}

// CENTER: select meter -
let elementSelect = document.querySelector('#meters');  // "select" tag with id="meters"

function changeOption() {
    let selectedOption = elementSelect.options[elementSelect.selectedIndex];  // need to see once more !!!!!!
    payment.meterId = selectedOption.innerHTML;
}

elementSelect.addEventListener('change', changeOption);

// CENTER: meter reading -
const inputPrevious = document.querySelector('#previous');
const inputCurrent = document.querySelector('#current');

const divFooter = document.querySelector('.form__footer');
const btnSave = divFooter.querySelector('.btn');
let previousValue;

inputPrevious.addEventListener('change', (event) => {
    previousValue = +event.target.value;

    if (previousValue > 0) {
        inputPrevious.style.borderColor = '#dee1e5';
        payment.previous = previousValue;
    } else {
        event.target.value = '';
        inputPrevious.style.borderColor = 'red';  //#dee1e5
        inputPrevious.setAttribute('placeholder', 'Значение больше 0');
    }
});

let currentValue;
inputCurrent.addEventListener('change', (e) => {
    currentValue = +e.target.value;

    if (currentValue > previousValue) {
        inputCurrent.style.borderColor = '#dee1e5';
        payment.current = currentValue;
    } else {
        e.target.value = '';
        inputCurrent.style.borderColor = 'red';  //#dee1e5
        inputCurrent.setAttribute('placeholder', `Должно быть > ${previousValue}`);
    }
});

// CENTER: Button "Save" -------------------------------------------------------------
let payments = JSON.parse(localStorage.getItem("MyPayments"));
if (payments == null) payments = [];

btnSave.addEventListener('click', (event) => {
    if (payment.meterId === undefined || payment.previous === undefined || payment.current === undefined || payment.id === undefined) {
        previousValue = '';
        currentValue = '';
        alert('Все поля должны быть учтены и заполнены, вкл. тип платежа (левое меню) и выбор счетчика.');
    } else {
        event.preventDefault();

        if (currentValue > previousValue) {

            inputCurrent.setAttribute('placeholder', ``);

            // onClick: calculate how much to pay --------------------------------------------
            let tariffForCalc = getTariff(payment.id);
            let toPay = (payment.current - payment.previous) * tariffForCalc;  // 30, number
            toPay = Math.round(toPay);
            payment.amount = toPay;     // store to ONE "payment"
            payments.push(payment);     // store to "payments"

            // onClick: create new element 'li' & add content --------------------------------
            let newLi = document.createElement('li');
            newLi.setAttribute('class', "list__item");
            newLi.innerHTML = `<p><span class=\"list__item-label\">${payment.meterId}</span>\n
                                  <span class=\"price\">$ <b>${toPay}</b></span></p>`;

            // onClick: CHANGE THE TOTAL SUM (ver.1) -
            // const bElementWithPrice = ulSummary.querySelector('.list__total .price b');  // <b>1 696</b>
            // let oldSum = Number((bElementWithPrice.innerHTML).split(' ').join(''));      // remove space & change type to Number => 1696
            // let newSum = oldSum + toPay;
            // bElementWithPrice.innerHTML = newSum.toLocaleString('ru-RU');                // add space between thousandth (see once more)


            // onClick: CHANGE THE TOTAL SUM (ver.2, with "reduce" method) --------------------
            const ulSummary = document.querySelector('.form__summary-list');
            const spanArrayPrice = ulSummary.querySelectorAll('.price b');
            let old = [];

            for (let s = 0; s < spanArrayPrice.length; s++) {
                let value = spanArrayPrice[s].innerHTML;   // 621, string
                const spam = "&nbsp;";

                if (value.includes(spam)) {
                    value = value.replace("&nbsp;", "");
                }
                if (value.includes(' ')) {
                    value = value.replace(" ", "");
                }
                old.push(Number(value));     // array from numbers, with "total"
            }

            let oldTotalNumber = old.pop();
            let newTotalNumber = old.reduce(((sum, curr) => sum + curr), toPay);  // string

            const bElementWithPrice = ulSummary.querySelector('.list__total .price b');  // getting element "b"
            bElementWithPrice.innerHTML = newTotalNumber.toLocaleString('ru-RU');  // change content, add space between thousandth (see once more)

            // onClick: add new element into DOM -----------------------------------------
            const targetComponent = document.querySelector('.list__total');
            const parentUl = targetComponent.parentNode;
            parentUl.insertBefore(newLi, targetComponent);   // *** pay attention ***

            // onClick: add to LOCAL STORAGE ----------------------------------------------
            localStorage.setItem("MyPayments", JSON.stringify(payments));

            // onClick: auto-check "service" on the right navigation panel ----------------
            autoCheckServiceRight(payment.id);

            // onClick: reset -------------------------------------------------------------
            noSelection();  // left side
            inputPrevious.value = '';  // center
            inputCurrent.value = '';   // center
            payment = {};
        }
    }
});

// RIGHT NAV.PANEL: Manually to uncheck the prepared payment -
uncheckServiceRight();

// RIGHT NAV.PANEL:Button "Pay" -
pay(payments);

// CENTER: Button "Clear" (Field: "К оплате") -
clearPayments(payments);





