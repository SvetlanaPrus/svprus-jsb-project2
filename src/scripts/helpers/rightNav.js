import {getTargetService} from "./getTargetService";

// Unmark all checkboxes -
export function removeAllMarksService(){
    const divRightNav = document.querySelector('.right__payments-fields');  // one "div" element
    const pElementsArray = divRightNav.getElementsByTagName('p');      // "p" array

    for(let i = 0; i < pElementsArray.length; i++) {
        let inputElement = pElementsArray[i].getElementsByTagName('input');
        inputElement[0].checked = false;
    }
}

// Automatically mark Service by pressing "Save" button -
export function autoCheckServiceRight(id){
    const divRightNav = document.querySelector('.right__payments-fields');  // one "div" element
    const pElementsArray = divRightNav.getElementsByTagName('p');      // "p" array

    let goal = getTargetService(id);   // => 'Налоги'

    for(let i = 0; i < pElementsArray.length; i++) {
        let content = pElementsArray[i].querySelector('span').innerHTML;

        if(content === goal){
            pElementsArray[i].getElementsByTagName('input')[0].checked = true;
        }
    }
}

// Manually uncheck Service -
export function uncheckServiceRight(){
    const divRightNav = document.querySelector('.right__payments-fields');  // one "div" element
    const pElements = divRightNav.getElementsByTagName('p');   // "p" array
    // const allInputs = divRightNav.getElementsByTagName('input');   // only for double check

    for(let i = 0; i < pElements.length; i++){

        pElements[i].addEventListener('click', () =>{

            let inputElement = pElements[i].getElementsByTagName('input');
            inputElement[0].checked = false;
            // console.log(allInputs);  // only for double check
        });
    }
}
















