import {objectTitlesRight} from "../constants";

// RIGHT NAVIGATION PANEL ("Saved payments") -
export function getTargetService(key){    // key = payment.id
    const array = Object.entries(objectTitlesRight);
    let targetValue = array.filter(el => el[0] === key).flat()[1];
    return targetValue;    // Возвращает строку, которую надо отметить => 'Налоги'
}
