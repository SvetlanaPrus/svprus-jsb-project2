import {getTargetService} from "./getTargetService";

export function checkStorage() {
    let payments = JSON.parse(localStorage.getItem("MyPayments"));

    if(payments !== null){
        let sum = 0;

        for (let i = 0; i < payments.length; i++) {

            sum = sum + payments[i].amount;

            // Перерисовывает дерево "Сохранённые платежи"
            let goal = getTargetService(payments[i].id);   // => 'Налоги'
            const pElementsRight = document.querySelectorAll('.right__payments-field');

            for(let s = 0; s < pElementsRight.length; s++){
                let name = pElementsRight[s].querySelector('label span').innerHTML;

                if(name === goal){
                    pElementsRight[s].querySelector('input').setAttribute('checked', 'true');
                }
            }

            // Перерисовывает дерево "К оплате".
            let newLi = document.createElement('li');
            newLi.setAttribute('class', "list__item");
            newLi.innerHTML = `<p><span class=\"list__item-label\">${payments[i].meterId}</span><span class=\"price\">$ <b>${payments[i].amount}</b></span></p>`;

            const targetComponent = document.querySelector('.list__total');
            const parentUl = targetComponent.parentNode;
            parentUl.insertBefore(newLi, targetComponent);
        }

        const totalPrice = document.querySelector('.list__item.list__total .price b');
        totalPrice.innerHTML = sum+'';
    }
}








