import {tarifs} from "../constants";

export function getTariff(key){
    const arrayFromTariffs = Object.entries(tarifs);
    let targetTariff = arrayFromTariffs.filter(el => el[0] === key).flat();
    return targetTariff[1];
}
