const divArray = document.getElementsByClassName("left__company", "left__company selected");  // this is ARRAY

// LEFT SIDE: reset selection -
export function noSelection() {
    for (let i = 0; i < divArray.length; i++) {
        divArray[i].setAttribute('class', "left__company");
    }
}
