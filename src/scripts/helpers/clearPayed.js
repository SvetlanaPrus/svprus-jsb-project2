import {getTargetService} from "./getTargetService";
import {removeAllMarksService} from "./rightNav";

/** Upon clicking button "Pay" - update Local Storage + update DOM: "К оплате" & "Сохранённые платежи" */

export function clearPayed(payedOkArray){   // getting array [{id:..., amount:...}]
    let payments = JSON.parse(localStorage.getItem("MyPayments"));   // array [{...}, {...}]; getting data from Local Storage

    // Remove ALL elements from DOM "К оплате", except "Total" -
    const ulFormSummaryList = document.querySelector('.form__summary-list');    // one "ul" element
    ulFormSummaryList.innerHTML = `<li class="list__item list__total">\n
         <p><span class="list__item-label">Всего</span>\n
             <span class="price">$ <b>0</b></span>\n
         </p>\n
     </li>`;

    // Remove ALL elements from DOM "Сохранённые платежи" -
    removeAllMarksService();

    // Remove all payments from Local Storage, which have been paid successfully -
    for(let i = 0; i < payedOkArray.length; i++){
        let idOk = payedOkArray[i].id;
        let amountOk = payedOkArray[i].amount;

        for(let j = 0; j < payments.length; j++){   // LS array
            let idLS = payments[j].id;
            let amountLS = payments[j].amount;

            if(idOk === idLS && amountOk === amountLS){
                payments.splice(j, 1);
            }
        }
    }
    localStorage.setItem("MyPayments", JSON.stringify(payments));

    // DOM rendering: "К оплате" -
    payments = JSON.parse(localStorage.getItem("MyPayments"));         // already updated array
    const targetComponent = document.querySelector('.list__total');
    let newElement = null, sum = 0;

    for(let k = 0; k < payments.length; k++){
        sum = sum + payments[k].amount;

        newElement = document.createElement('li');
        newElement.setAttribute('class', "list__item");
        newElement.innerHTML = `<p><span class=\"list__item-label\">${payments[k].meterId}</span>\n
                                         <span class=\"price\">$ <b>${payments[k].amount}</b></span></p>`;

        const parentUl = targetComponent.parentNode;
        parentUl.insertBefore(newElement, targetComponent);
    }
    targetComponent.querySelector('.price b').innerHTML = sum+'';
    localStorage.setItem("MyPayments", JSON.stringify(payments));


    // DOM rendering: "Сохранённые платежи" -
    payments = JSON.parse(localStorage.getItem("MyPayments"));         // already updated array
    const arrayFromPElements = document.getElementsByClassName('right__payments-field');

    for(let f = 0; f < arrayFromPElements.length; f++){
        let contentFromPElement = arrayFromPElements[f].querySelector('span').innerHTML;  // 'Налоги'

        for(let m = 0; m < payments.length; m++){
            let id = payments[m].id;
            let contentFromPayment = getTargetService(id);  // get => 'Налоги'

            if(contentFromPElement === contentFromPayment){
                arrayFromPElements[f].getElementsByTagName('input')[0].checked = true;
            }
        }
    }
    localStorage.setItem("MyPayments", JSON.stringify(payments));
}
