// BUTTON "CLEAR" -
import {removeAllMarksService} from "./rightNav";

const divFooter = document.querySelector('.form__footer');
const btnClear = divFooter.querySelector('.btn-outline');    // button
const ulFormSummaryList = document.querySelector('.form__summary-list');    // one "ul" element
const liElementsListItem = ulFormSummaryList.getElementsByClassName('list__item');    // "li" array
const bElementWithTotalPrice = ulFormSummaryList.querySelector('.list__total .price b');    // getting element "b"

/** NEED TO BE CHECKED!
 * Upon click, all items are deleted, except one. Why?
 * With another click - this last item can also be deleted without problems.
 * */

export function clearPayments(payments) {
    btnClear.addEventListener('click', () => {
        payments = [];            // - OK
        localStorage.clear();     // clear Local Storage - OK

        for (let i = 0; i < liElementsListItem.length; i++) {         // remove elements from DOM
            let isListTotal = liElementsListItem[i].getAttribute('class');
            if (!isListTotal.includes('list__total')){
                console.log(isListTotal);
                liElementsListItem[i].remove();
            }
        }
        bElementWithTotalPrice.innerHTML = (0).toLocaleString('ru-RU');   // CENTER: clear "Total Sum" - OK
        removeAllMarksService();   // RIGHT: clear "saved payments" - OK
    });
}

export function clearPaymentsAuto(){
    for (let i = 0; i < liElementsListItem.length; i++) {
        let isListTotal = liElementsListItem[i].getAttribute('class');
        if (!isListTotal.includes('.list__total')) liElementsListItem[i].remove();
    }
    bElementWithTotalPrice.innerHTML = (0).toLocaleString('ru-RU');
}

