// Button "Pay" -
import {objectTitlesRight} from "../constants";
import {clearPayed} from "./clearPayed";

export function pay(payments) {
    const divPay = document.querySelector('.right__payments-footer');
    const btnPay = divPay.querySelector('.btn');          // button
    const divRightNav = document.querySelector('.right__payments-fields');  // one "div" element
    const pElements = divRightNav.getElementsByTagName('p');   // "p" array
    const ulElement = document.querySelector('.transactions__list');  // one "ul" element

    let balance = 500, status, paymentsOk = [];

    const feedback = {
        OK: 'успешно оплачено',
        ERROR: 'ошибка транзакции'
    };

    ulElement.innerHTML = null;

    btnPay.addEventListener('click', (event) => {
        event.preventDefault();

        for (let i = 0; i < pElements.length; i++) {

            let input = pElements[i].getElementsByTagName('input');
            let isCheck = input[0].checked;

            if (isCheck === true) {
                let span = pElements[i].getElementsByTagName('span');
                let serviceName = span[0].innerHTML;   // => 'Интернет'

                // Return "key" from object by sending "value"  => 'internet' -
                let serviceId = Object.entries(objectTitlesRight).find(([, name]) => serviceName === name);

                for (let j = 0; j < payments.length; j++) {
                    if (payments[j].id === serviceId[0]) {
                        balance = balance - payments[j].amount;

                        if (balance > 0) {
                            status = feedback.OK;
                            paymentsOk.push({
                                id: payments[j].id,
                                amount: payments[j].amount
                            });
                            console.log(`${serviceName}: оплачено`);
                        } else {
                            status = feedback.ERROR;
                        }
                    }
                }

                // onClick: create new element 'li' & add content -
                let newElement = document.createElement('li');

                if(status === feedback.OK){
                    newElement.setAttribute('class', 'list__item');
                } else {
                    newElement.setAttribute('class', 'list__item list__item-error');
                }
                newElement.innerHTML = `${serviceName}: ${status}`;
                ulElement.appendChild(newElement);
            }
        }
        clearPayed(paymentsOk);

        btnPay.setAttribute('disabled', 'true');
    });
}

