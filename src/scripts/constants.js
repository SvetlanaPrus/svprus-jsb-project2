export const tarifs = Object.freeze({
    taxes: 0.2,
    water: 0.3,
    internet: 0.4,
    security: 0.5,
    exchange: 0.6,
});

export const objectTitles = {
    taxes: 'Оплата услуги: налоги',
    water: 'Оплата услуги: холодное водоснабжение',
    internet: 'Оплата услуги: интернет',
    security: 'Оплата услуги: охрана',
    exchange: 'Оплата услуги: обмен валют'
};

export const objectTitlesRight = {
    taxes: 'Налоги',
    water: 'Холодная вода',
    internet: 'Интернет',
    security: 'Охрана',
    exchange: 'Обмен валют'
};
